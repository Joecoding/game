'use strict';
const player0El = document.querySelector('.player--0');
const player1El = document.querySelector('.player--1');
const score0El = document.querySelector('#score--0');
const score1El = document.querySelector('#score--1');
const diceEl = document.querySelector('.dice');
const btnRoll = document.querySelector('.btn--roll');
const btnNew = document.querySelector('.btn--new');
const btnHold = document.querySelector('.btn--hold');
const btnScore0El = document.querySelector('#current--0');
const btnScore1El = document.querySelector('#current--1');





//Hidden Connect ans Starting condition
score0El.textContent = 0;
score1El.textContent = 0
diceEl.classList.add('hidden');
let activePlayer = 0;
let currentScore = 0 ;
const scores = [0,0];
let playing = true;

const switchPlayer  = function ()  {
    document.getElementById(`current--${activePlayer}`).textContent = 0;
    currentScore = 0;
    activePlayer = activePlayer === 0? 1 : 0;
    player0El.classList.toggle('player--active');
    player1El.classList.toggle('player--active');

}

//Rolling dice functionality
btnRoll.addEventListener('click', function () {
    // 1. Generating rolling dice
    if(playing) {

    const dice = Math.trunc(Math.random() * 6) + 1;
    

    // 2. Display dice
    diceEl.classList.remove('hidden');
    diceEl.src = `dice-${dice}.png`;

    // 3. Check for rolled 1: if true , switch to next player
      if( dice !== 1){
        currentScore += dice;
        document.getElementById(`current--${activePlayer}`).textContent = currentScore;
        //btnScore0El.textContent = currentScore;

      }else {

        switchPlayer();

      }

 }
});

btnHold.addEventListener('click', function() {
    //Add score to totalscore 
    if(playing) {
   scores[activePlayer] += currentScore;
   document.getElementById(`score--${activePlayer}`).textContent = scores[activePlayer];

    //check if score >=100
    if( scores[activePlayer] >= 20){
        diceEl.classList.add('hidden');
        playing = false;
        document.querySelector(`.player--${activePlayer}`).classList.add('player--winner');
        document.querySelector(`.player--${activePlayer}`).classList.remove('player--active');

    } else{
       //switch the player
    switchPlayer();
    }

}

});

btnNew.addEventListener('click', function() {
    location.reload();
})

 
